#ifndef SELECTFIRMWAREDIALOG_H
#define SELECTFIRMWAREDIALOG_H

#include <QDialog>
#include <QFileDialog>

namespace Ui {
class SelectFirmwareDialog;
}

class SelectFirmwareDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SelectFirmwareDialog(QMap<int, QString> typeList, QString lastPath, QWidget *parent = 0);
    ~SelectFirmwareDialog();
    struct Firmware{
      QString uuid;
      int deviceType;
      bool needMonitor;
      int hardVersion;
      int softVersion;
      int monitorSoftVersion;
      int loaderSoftVersion;
      int groupCount;
      QString battlePath;
      QString monitorPath;
      QString tehMonitorPath;
      QString loaderPath;
      QString localBattlePath;
      QString localMonitorPath;
      QString localTehMonitorPath;
      QString localLoaderPath;
    };

private:
    Ui::SelectFirmwareDialog *ui;
    QString firmwarePath;
private slots:
    void disableMonitorFields(int index);
    void on_battlePathOpenButton_clicked();
    void on_monitorOpenButton_clicked();
    void on_tehMonitorOpenButton_clicked();
    void on_loaderPathOpenButton_clicked();

signals:
    void pathChanged(QString path);
    void firmwareSelected(Firmware firmware);
};

#endif // SELECTFIRMWAREDIALOG_H

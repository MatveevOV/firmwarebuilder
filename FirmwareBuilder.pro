#-------------------------------------------------
#
# Project created by QtCreator 2017-09-18T11:18:23
#
#-------------------------------------------------

# Инструкция для прошиватора
#
# 1. Запустить FirmwareBuilder из Qt Creator
# 2. Ввести название проекта
# 3. Добавить прошивку
# 3.1 Выбрать тип, количество мест в групповом устройстве
# 3.2 Аппаратную версию, версию ПО и .hcs получить у разработчика
# 3.3 При выборе загрузки монитора нужно добавить .hcs монитора и технологического монитора, для ГК еще нужен .hcs загрузчика
# 4. Добавить еще прошивки
# 5. Собрать, откроется каталог сборки "E:\projects\FirmwareUpdater\build-static-manual"


QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FirmwareBuilder
TEMPLATE = app
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    selectfirmwaredialog.cpp

HEADERS += \
        mainwindow.h \
    selectfirmwaredialog.h

FORMS += \
        mainwindow.ui \
    selectfirmwaredialog.ui

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QDebug>
#include <QFile>
#include <QDate>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QDialog>
#include <QModelIndex>
#include <QComboBox>
#include <QRadioButton>
#include <QButtonGroup>
#include <QDialogButtonBox>
#include <QSpinBox>
#include <QUuid>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>


#include "selectfirmwaredialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    enum firmwareType{
        GK = 0x1021,
        KAU = 0x1022,
        PMF = 0x1023,
        IPR = 0xd8,
        IP2D = 0xd9,
        MR = 0xda,
        MA = 0xdb,
        MDU220 = 0xdc,
        IPD = 0xdd,
        IPT = 0xde,
        IPK = 0xdf,
        PPU_DN = 0xe0,
        MAP = 0xe1,
        MVK = 0xe2,
        OPS = 0xe3,
        OPK = 0xe4,
        MVP = 0xe5,
        OPZ = 0xe6,
        NK = 0xe7,
        KV = 0xe8,
        NSCH = 0xe9,
        MDU24 = 0xea,
        OPKS = 0xeb,
        OPKZ = 0xec,
        PDU = 0xee,
        IOV = 0xef,
        UDP = 0xf0,
        ZOV = 0xf1,
        SCOPA = 0xf2,
        KDKR = 0xf3,
        IS = 0xf8,
        BMP = 0xf9,
        IOIK = 0x10,
        IOPZ = 0x11,
        ABPC = 0x12,
        ABSHS = 0x13,
        ABTK = 0x14,
        MRK = 0x15,
        IPRE = 0x16,
        IPTE = 0x17,
        IPR_RK = 0x18,
        KPP_RK = 0x19,
        MR_RK = 0x1a,
        MA_RK = 0x1b,
        IPD_RK = 0x1d,
        IPT_RK = 0x1e,
        IOLIT = 0x20,
        IOIK_RK = 0x21,
        IOPZ_RK = 0x22,
        OPS_RK = 0x23,
        OPK_RK = 0x24,
        MI = 0x25,
        OPZ_RK = 0x26,
        RR_RK = 0x27,
        IOV_RK = 0x28,
        KT_RK = 0x2b,
        BU_RK = 0x2a,
        PPU_ZHN = 0x2,
        PPU_PN = 0x3,
        PPU_V = 0x4,
        PPU_Z_KV_BAES = 8,
        PPU_Z_KV_MV_BAES  = 9,
        PPU_Z = 0xb,
        PPU_Z_MV = 0xc,
        PPU_Z_DU = 0xd,
        MAV = 0xd3,
    };

    ~MainWindow();

private slots:
    //сборка проекта
    void on_buildButton_clicked();
    //добавить прошивку в списко
    void on_addFirmwareButton_clicked();
    //удаление прошивки из списка
    void removeFirmware(QModelIndex index);
    //очистка папки со сборкой
    void cleanOutput();
    //добавление названия проекта в pro файл исходников прошиватора
    void addFileDescription();
    //добавить прошивку в список
    void addFirmware(SelectFirmwareDialog::Firmware firmware);
    //копировать все выбранные прошивки в папку с исходниками прошиватора
    bool addFirmwaresToQrc();
    //очистить qrc и папку с прошивками в исходниках
    void clearFirmwares();
    //создать файл qrc с описанием всех файлов прошивок
    void createQrc(QFileInfoList fileNames);
    //вывод этапов сборки в консоль отладки
    void consoleOutput();
    //проверка указанных путей на наличие qmake, mingw, pro файла, наличие выбранных прошивок
    bool updatePath(); //true, если нет ошибок
    //заполнение мапа с именами прошивок
    void fillFirmwareNameMap();

    void saveFirmwaresDescription(QJsonArray &firmwaresArray);

    void on_openFirmwareDescroptionButton_released();
    void  showOpenedFirmwares();


private:
    Ui::MainWindow *ui;
    //пути
    QString qtPath;
    QString mingwPath;
    QString qmakePath;
    QString mingw;
    QString pathVariable;
    QString qtdirVariable;
    QString sourcePath;
    QString destinationPath;
    QString proFilePath;
    QString bufferProFilePath;
    QString firmwarePath;
    //все выбранные прошивки
    QMap<QString, SelectFirmwareDialog::Firmware> selectedFirmwares;
    //диалог выбора прошивки
    SelectFirmwareDialog *selectFirmwareDialog;
    //мап с именами прошивок
    QMap <int, QString> firmwareNameMap;
    //процесс для запуска сборки из консоли
    QProcess *process;

public slots:
    void saveFirmwarePath(QString path);


};

#endif // MAINWINDOW_H

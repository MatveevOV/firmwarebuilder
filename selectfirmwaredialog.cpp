#include "selectfirmwaredialog.h"
#include "ui_selectfirmwaredialog.h"

SelectFirmwareDialog::SelectFirmwareDialog(QMap<int, QString> typeList, QString lastPath, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectFirmwareDialog)
{
    ui->setupUi(this);

    setMinimumSize(QSize(400,200));
    setMaximumSize(QSize(800,400));
    setAttribute(Qt::WA_DeleteOnClose);
    setWindowModality(Qt::ApplicationModal);

    firmwarePath = lastPath;

    auto iter = typeList.begin();
    for (; iter != typeList.end(); ++iter)
        ui->deviceTypeComboBox->addItem(iter.value());


    ui->groupCountSpinBox->setRange(1,32);

    ui->firmwareTypeComboBox->addItem("Боевая программа");
    ui->firmwareTypeComboBox->addItem("Монитор");

    ui->battleHardVerSpinBox->setRange(1,255);
    ui->battleSoftVerSpinBox->setRange(1,255);
    ui->monitorSoftVerSpinBox->setRange(1,255);

    ui->loaderSoftVerSpinBox->setRange(1,255);


    connect(ui->okButton, &QPushButton::clicked, this, [=](){
        Firmware firmware;
        firmware.deviceType = typeList.key(ui->deviceTypeComboBox->currentText());
        firmware.groupCount = ui->groupCountSpinBox->value();
        firmware.needMonitor = ui->firmwareTypeComboBox->currentIndex();
        firmware.battlePath = ui->battlePathEdit->text();
        firmware.hardVersion = ui->battleHardVerSpinBox->value();
        firmware.softVersion = ui->battleSoftVerSpinBox->value();
        firmware.monitorPath = ui->monitorPathEdit->text();
        firmware.monitorSoftVersion = ui->monitorSoftVerSpinBox->value();
        firmware.tehMonitorPath = ui->tehMonitorPathEdit->text();
        firmware.loaderPath = ui->loaderPathEdit->text();
        firmware.loaderSoftVersion = ui->loaderSoftVerSpinBox->value();

        emit firmwareSelected(firmware);});
    connect(ui->cancelButton, &QPushButton::clicked, this, &QDialog::reject);
    connect(ui->firmwareTypeComboBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
            this, &SelectFirmwareDialog::disableMonitorFields);

    ui->monitorSoftVerSpinBox->setEnabled(false);
    ui->monitorPathEdit->setEnabled(false);
    ui->monitorOpenButton->setEnabled(false);
    ui->tehMonitorPathEdit->setEnabled(false);
    ui->tehMonitorOpenButton->setEnabled(false);

    ui->loaderSoftVerSpinBox->setEnabled(false);
    ui->loaderPathEdit->setEnabled(false);
    ui->loaderPathOpenButton->setEnabled(false);


}

SelectFirmwareDialog::~SelectFirmwareDialog()
{
    delete ui;
}

void SelectFirmwareDialog::disableMonitorFields(int index)
{
    ui->monitorSoftVerSpinBox->setEnabled(index);
    ui->monitorPathEdit->setEnabled(index);
    ui->monitorOpenButton->setEnabled(index);

    ui->tehMonitorPathEdit->setEnabled(index);
    ui->tehMonitorOpenButton->setEnabled(index);

    ui->loaderSoftVerSpinBox->setEnabled(index);
    ui->loaderPathEdit->setEnabled(index);
    ui->loaderPathOpenButton->setEnabled(index);

}

void SelectFirmwareDialog::on_battlePathOpenButton_clicked()
{
    ui->battlePathEdit->setText(QFileDialog::getOpenFileName(this,
                                                           tr("Открыть рабочую прошивку"), firmwarePath, tr("HCS Files (*.hcs)")));
    firmwarePath = ui->battlePathEdit->text();
    emit pathChanged(firmwarePath);
}

void SelectFirmwareDialog::on_monitorOpenButton_clicked()
{
    ui->monitorPathEdit->setText(QFileDialog::getOpenFileName(this,
                                                           tr("Открыть монитор"), firmwarePath, tr("HCS Files (*.hcs)")));
    firmwarePath = ui->monitorPathEdit->text();
    emit pathChanged(firmwarePath);

}

void SelectFirmwareDialog::on_tehMonitorOpenButton_clicked()
{
    ui->tehMonitorPathEdit->setText(QFileDialog::getOpenFileName(this,
                                                           tr("Открыть технологический монитор"), firmwarePath, tr("HCS Files (*.hcs)")));
    firmwarePath = ui->tehMonitorPathEdit->text();
    emit pathChanged(firmwarePath);

}

void SelectFirmwareDialog::on_loaderPathOpenButton_clicked()
{
    ui->loaderPathEdit->setText(QFileDialog::getOpenFileName(this,
                                                           tr("Открыть загрузчик"), firmwarePath, tr("HCS Files (*.hcs)")));
    firmwarePath = ui->loaderPathEdit->text();
    emit pathChanged(firmwarePath);
}

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    process = new QProcess(this);
    firmwarePath = "./";
    fillFirmwareNameMap();
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(4, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(6, QHeaderView::ResizeToContents);
    ui->firmwareTable->horizontalHeader()->setSectionResizeMode(7, QHeaderView::Stretch);
    connect(process, &QProcess::readyReadStandardOutput, this, &MainWindow::consoleOutput);
    connect(ui->firmwareTable, &QTableWidget::doubleClicked, this, &MainWindow::removeFirmware);


}

MainWindow::~MainWindow()
{
    delete ui;
}
bool MainWindow::updatePath()
{
    sourcePath = ui->sourceLineEdit->text();
    destinationPath = ui->destinationLineEdit->text();
    qtPath = ui->qtPathEdit->text();
    mingwPath = ui->mingwPathEdit->text();

    qmakePath = qtPath + "\\qmake.exe";
    mingw = mingwPath + "\\mingw32-make.exe";

    proFilePath =  sourcePath +"\\FirmwareUpdater.pro";
    bufferProFilePath = sourcePath + "\\Buffer.pro";

    pathVariable = qtPath + ";" + mingwPath + ";";
    //без bin
    qtdirVariable = qtPath.mid(0,qtPath.length() - 4) + ";";
    QString warningMessage;
    QFile checkFile(qmakePath);
    if (!checkFile.exists())
        warningMessage.append("- Неверно указан путь к qmake\n");
    checkFile.setFileName(mingw);
    if (!checkFile.exists())
        warningMessage.append("- Неверно указан путь к mingw\n");
    checkFile.setFileName(proFilePath);
    if (!checkFile.exists())
        warningMessage.append("- Отсутствует файл проекта\n");
    if (selectedFirmwares.isEmpty())
        warningMessage.append("- Не выбрано ни одной прошивки\n");
    if (!warningMessage.isEmpty())
    {
        QMessageBox msgBox;
        warningMessage.prepend("Обнаружены ошибки:\n");
        msgBox.setText(warningMessage);
        msgBox.exec();
        return false;
    }
    return true;
}

void MainWindow::fillFirmwareNameMap()
{
    firmwareNameMap.insert(GK, "ГК");
    firmwareNameMap.insert(KAU, "КАУ");
    firmwareNameMap.insert(PMF, "ПМФ");
    firmwareNameMap.insert(IPR, "ИПР");
    firmwareNameMap.insert(MR, "МР");
    firmwareNameMap.insert(MA,  "МА");
    firmwareNameMap.insert(MDU220,"МДУ220");
    firmwareNameMap.insert(IPD, "ИПД");
    firmwareNameMap.insert(IPT, "ИПТ");
    firmwareNameMap.insert(IPK, "ИПК");
    firmwareNameMap.insert(PPU_DN,"ППУ-ДН");
    firmwareNameMap.insert(MAP,"МАП");
    firmwareNameMap.insert(MVK, "МВК");
    firmwareNameMap.insert(OPS, "ОПС");
    firmwareNameMap.insert(OPK, "ОПК");
    firmwareNameMap.insert(MVP, "МВП");
    firmwareNameMap.insert(OPZ, "ОПЗ");
    firmwareNameMap.insert(NK, "НК");
    firmwareNameMap.insert(KV,"КВ");
    firmwareNameMap.insert(NSCH, "НСЧ");
    firmwareNameMap.insert(MDU24, "МДУ24");
    firmwareNameMap.insert(OPKS, "ОПКС");
    firmwareNameMap.insert(OPKZ, "ОПКЗ");
    firmwareNameMap.insert(PDU, "ПДУ");
    firmwareNameMap.insert(IOV,"ИОВ");
    firmwareNameMap.insert(UDP,"УДП");
    firmwareNameMap.insert(ZOV,"ЗОВ");
    firmwareNameMap.insert(SCOPA,"СКОПА");
    firmwareNameMap.insert(KDKR,"КДКР");
    firmwareNameMap.insert(IS,"ИС");
    firmwareNameMap.insert(BMP,"БМП");
    firmwareNameMap.insert(IOIK,"ИОИК");
    firmwareNameMap.insert(IOPZ,"ИОПЗ");
    firmwareNameMap.insert(ABPC,"АБПЦ");
    firmwareNameMap.insert(ABSHS,"АБШС");
    firmwareNameMap.insert(ABTK,"АБТК");
    firmwareNameMap.insert(MRK,"МРК");
    firmwareNameMap.insert(IPRE,"ИПРЭ");
    firmwareNameMap.insert(IPTE,"ИПТЭ");
    firmwareNameMap.insert(IPR_RK,"ИПР-РК");
    firmwareNameMap.insert(KPP_RK,"КПП-РК");
    firmwareNameMap.insert(MR_RK,"МР-РК");
    firmwareNameMap.insert(MA_RK,"МА-РК");
    firmwareNameMap.insert(IPD_RK,"ИПД-РК");
    firmwareNameMap.insert(IPT_RK,"ИПТ-РК");
    firmwareNameMap.insert(IOLIT,"ИОЛИТ");
    firmwareNameMap.insert(IOIK_RK,"ИОИК-РК");
    firmwareNameMap.insert(IOPZ_RK,"ИОПЗ-РК");
    firmwareNameMap.insert(OPS_RK,"ОПС-РК");
    firmwareNameMap.insert(OPK_RK,"ОПК-РК");
    firmwareNameMap.insert(MI,"МИ");
    firmwareNameMap.insert(OPZ_RK,"ОПЗ-РК");
    firmwareNameMap.insert(RR_RK,"РР-РК");
    firmwareNameMap.insert(IOV_RK,"ИОВ-РК");
    firmwareNameMap.insert(KT_RK,"КТ-РК");
    firmwareNameMap.insert(BU_RK,"БУ-РК");
    firmwareNameMap.insert(PPU_ZHN,"ППУ-ЖН");
    firmwareNameMap.insert(PPU_PN,"ППУ-ПН");
    firmwareNameMap.insert(PPU_V,"ППУ-В");
    firmwareNameMap.insert(PPU_Z_KV_BAES,"ППУ-З-КВ-БАЭС");
    firmwareNameMap.insert(PPU_Z_KV_MV_BAES,"ППУ-З-КВ-МВ-БАЭС");
    firmwareNameMap.insert(PPU_Z,"ППУ-З");
    firmwareNameMap.insert(PPU_Z_MV,"ППУ-З-МВ");
    firmwareNameMap.insert(PPU_Z_DU,"ППУ-З-ДУ");
    firmwareNameMap.insert(MAV,  "МАВ");


}

void MainWindow::saveFirmwaresDescription(QJsonArray &firmwaresArray)
{
    auto iter = selectedFirmwares.begin();
    for (; iter != selectedFirmwares.end(); ++iter)
    {
        QJsonObject firmware;
        firmware["deviceType"] = iter.value().deviceType;
        firmware["groupCount"] = iter.value().groupCount;
        firmware["needMonitor"] = iter.value().needMonitor;
        firmware["battlePath"] = iter.value().localBattlePath;
        firmware["hardVersion"] = iter.value().hardVersion;
        firmware["softVersion"] = iter.value().softVersion;
        firmware["monitorPath"] = iter.value().localMonitorPath;
        firmware["loaderSoftVersion"] = iter.value().loaderSoftVersion;
        firmware["monitorSoftVersion"] = iter.value().monitorSoftVersion;
        firmware["tehMonitorPath"] = iter.value().localTehMonitorPath;
        firmware["systemBattlePath"] = iter.value().battlePath;
        firmware["systemMonitorPath"] = iter.value().monitorPath;
        firmware["systemTehMonitorPath"] = iter.value().tehMonitorPath;
        firmware["loaderPath"] = iter.value().localLoaderPath;
        firmware["systemLoaderPath"] = iter.value().loaderPath;


        firmwaresArray.append(firmware);
    }
}

void MainWindow::saveFirmwarePath(QString path)
{
    firmwarePath = path;
}

void MainWindow::on_buildButton_clicked()
{
    if (!updatePath())
        return;

    cleanOutput();

    addFileDescription();
    if (!addFirmwaresToQrc())
        return;

    //первые два символа из пути - диск в винде нужно сначала перейти на диск
    QString disk = destinationPath.mid(0,2);
    //переход в папку для сборки
    QString cd = QString("cd %1").arg("\"" + destinationPath + "\"");
    //установка переменных среды
    QString setPath = QString("set PATH=%1 && set QTDIR=%2").arg(pathVariable)
                                                            .arg(qtdirVariable);
    //создание qmake файла
    QString qmake = QString("%1 %2 -spec win32-g++ && %3 qmake_all").arg(qmakePath)
                                                                    .arg("\"" + proFilePath + "\"")
                                                                    .arg(mingw);
    //компиляция с помощью make файла
    QString make = mingw;
    //переход в папку с собранным проектом и ее открытие
    QString cdRelease = QString("cd %1\\release").arg("\"" + destinationPath + "\"");
    QString openDir = "start .";

    QString buildCommand = QString(disk + " && " + cd + " && " + setPath + " && " +
                                   qmake + " && " + make + " && " + cdRelease + " && " + openDir);

    process->execute("cmd /C" + buildCommand);

    //удаление буферного pro file

    QFile::remove(proFilePath);
    QFile::rename(bufferProFilePath, proFilePath);

    clearFirmwares();


}

void MainWindow::addFileDescription()
{
    //создается буферный pro файл, в который добавляется название проекта, компиляция будет вестись с помощью него
    QFile proFile(proFilePath);
    proFile.copy(bufferProFilePath);
    if (proFile.open(QIODevice::Text|QIODevice::Append))
    {
        QString targetName = QString("%1_Updater_%2").arg(ui->projectNameEdit->text()).arg(QDate::currentDate().toString("dd-MM-yyyy"));
        QByteArray description = QString("RESOURCES += \
                                         firmwares.qrc \n"
                                         "TARGET = %1 \n"
                                         "QMAKE_TARGET_PRODUCT = %1 \n"
                                         "QMAKE_TARGET_DESCRIPTION = %1").arg(targetName).toLocal8Bit();
        proFile.write(description);
    }

}

bool MainWindow::addFirmwaresToQrc()
{

    QString firmwarePathInResources = sourcePath + "\\firmwares";
    QDir dir;
    dir.mkdir(firmwarePathInResources);
    dir.cd(firmwarePathInResources);
    QString warningText;
    auto iter = selectedFirmwares.begin();
    for (; iter != selectedFirmwares.end(); ++iter)
    {
        SelectFirmwareDialog::Firmware firmware = iter.value();
        QFileInfo fi(firmware.battlePath);


        QString localBattlePath = firmwarePathInResources + "\\" + fi.fileName();
        QFile::copy(firmware.battlePath, localBattlePath);
        iter.value().localBattlePath = fi.fileName();

        if (!fi.exists())
        {
            warningText.append(firmware.battlePath);
            warningText.append("\n");
        }

        QFileInfo fiMon(firmware.monitorPath);
        if (fiMon.exists())
        {
            QString localMonitorPath = firmwarePathInResources + "\\" + fiMon.fileName();
            QFile::copy(firmware.monitorPath, localMonitorPath);
            iter.value().localMonitorPath = fiMon.fileName();

        }

        QFileInfo fiTeh(firmware.tehMonitorPath);
        if (fiTeh.exists())
        {
            QString localTehMonitorPath = firmwarePathInResources + "\\" + fiTeh.fileName();
            QFile::copy(firmware.tehMonitorPath, localTehMonitorPath);
            iter.value().localTehMonitorPath = fiTeh.fileName();

        }

        QFileInfo fiLoader(firmware.loaderPath);
        if (fiLoader.exists())
        {
            QString localLoaderPath = firmwarePathInResources + "\\" + fiLoader.fileName();
            QFile::copy(firmware.loaderPath, localLoaderPath);
            iter.value().localLoaderPath = fiLoader.fileName();

        }

        if (firmware.needMonitor)
        {
            if (!fiMon.exists())
            {
                warningText.append(fiMon.absoluteFilePath());
                warningText.append("\n");
            }
            if (!fiTeh.exists())
            {
                warningText.append(fiTeh.absoluteFilePath());
                warningText.append("\n");
            }
            if (!fiLoader.exists() && !firmware.loaderPath.isEmpty())
            {
                warningText.append(fiLoader.absoluteFilePath());
                warningText.append("\n");
            }
        }

    }

    QMessageBox msgBox;
    if (!warningText.isEmpty())
    {
        warningText.prepend("Неверно указан путь прошивки:\n");
        msgBox.setText(warningText);
        msgBox.exec();
        return false;
    }

    QFile saveFile("description.json");

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }

    QJsonArray firmwares;
    saveFirmwaresDescription(firmwares);
    QJsonDocument saveDoc(firmwares);
    saveFile.write(saveDoc.toJson());
    saveFile.close();

    //скопировать в папку с исходниками
    QFileInfo descFi(saveFile);
    QFile::copy(descFi.absoluteFilePath(), QString(firmwarePathInResources + "\\" + descFi.fileName()));

    //скопировать в папку с собранным проектом
    QString builderSaveFilePath = QString(destinationPath + "\\" + ui->projectNameEdit->text() + "." + descFi.completeSuffix());
    if (QFile(builderSaveFilePath).exists())
        QFile::remove(builderSaveFilePath);
    QFile::copy(descFi.absoluteFilePath(), builderSaveFilePath);


    createQrc(dir.entryInfoList(QDir::Files));


    return true;

}

void MainWindow::clearFirmwares()
{
    QFile qrc(QString(sourcePath + "\\firmwares.qrc"));
    QDir firmwares(QString(sourcePath + "\\firmwares"));
    qrc.remove();
    firmwares.removeRecursively();
}

void MainWindow::createQrc(QFileInfoList fileNames)
{
    QFile qrc(QString(sourcePath + "\\firmwares.qrc"));

    if(qrc.open(QIODevice::ReadWrite))
    {
        qrc.write(QString("<RCC>\n").toLocal8Bit());
        qrc.write(QString("<qresource prefix= '/' >\n").toLocal8Bit());
        foreach (QFileInfo file, fileNames) {
            qrc.write(QString("<file>firmwares/%1</file>\n").arg(file.fileName()).toLocal8Bit());
        }
        qrc.write(QString("</qresource>\n").toLocal8Bit());
        qrc.write(QString("</RCC>\n").toLocal8Bit());


    }
    qrc.close();
}

void MainWindow::consoleOutput()
{
    qDebug() << process->readAllStandardOutput();
    process->close();
}

void MainWindow::on_addFirmwareButton_clicked()
{
    selectFirmwareDialog = new SelectFirmwareDialog(firmwareNameMap, firmwarePath);
    connect(selectFirmwareDialog, &SelectFirmwareDialog::firmwareSelected, this, &MainWindow::addFirmware);
    connect(selectFirmwareDialog, &SelectFirmwareDialog::pathChanged, this, &MainWindow::saveFirmwarePath);
    selectFirmwareDialog->show();
}

void MainWindow::addFirmware(SelectFirmwareDialog::Firmware firmware)
{
    QFile battleFile(firmware.battlePath);
    QFile monitorFile(firmware.monitorPath);
    QFile tehMonitorFile(firmware.tehMonitorPath);
    QFile loaderFile(firmware.loaderPath);



    if (firmware.needMonitor)
    {
        if (firmware.deviceType == GK)
        {
            if (!(battleFile.exists() && monitorFile.exists() && tehMonitorFile.exists() && loaderFile.exists()))
            {
                qDebug() << "gk";
                QMessageBox msgBox;
                msgBox.setText("Файл не выбран");
                msgBox.exec();
                return;
            }
        }

        else
        {
            if (!(battleFile.exists() && monitorFile.exists() && tehMonitorFile.exists()))
            {
                QMessageBox msgBox;
                msgBox.setText("Файл не выбран");
                msgBox.exec();
                return;
            }
        }

    }
    else
    {
        //очистить поля, если они были заполнены, а потом выбран режим только боевой прошивки
        firmware.monitorPath.clear();
        firmware.tehMonitorPath.clear();
        firmware.loaderPath.clear();

        if (!battleFile.exists())
        {
            QMessageBox msgBox;
            msgBox.setText("Файл не выбран");
            msgBox.exec();
            return;

        }
    }

    //найти тип устройства, аппаратная версия и количество мест в списке уже добавленных прошивок
    auto iter = selectedFirmwares.begin();
    for (; iter != selectedFirmwares.end(); ++iter)
    {
        if (iter.value().deviceType == firmware.deviceType &&
                iter.value().groupCount == firmware.groupCount &&
                iter.value().hardVersion == firmware.hardVersion)
            firmware.uuid = iter.value().uuid;
    }

    //если прошивка уже есть в списке, изменить путь на выбранный
    if (firmware.uuid.isEmpty())
    {
        QString uuid = QUuid::createUuid().toString();
        firmware.uuid = uuid;

        ui->firmwareTable->setRowCount(ui->firmwareTable->rowCount()+1);
        qDebug() << firmware.deviceType << firmware.uuid;
        QTableWidgetItem *typeItem = new QTableWidgetItem(firmwareNameMap.value(firmware.deviceType));
        typeItem->setData(Qt::UserRole, firmware.uuid);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 0, typeItem);

        QTableWidgetItem *groupItem = new QTableWidgetItem(QString::number(firmware.groupCount));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 1, groupItem);

        QTableWidgetItem *battlepathItem = new QTableWidgetItem(firmware.battlePath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 2, battlepathItem);

        QTableWidgetItem *hardItem = new QTableWidgetItem(QString::number(firmware.hardVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 3, hardItem);

        QTableWidgetItem *softItem = new QTableWidgetItem(QString::number(firmware.softVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 4, softItem);

        QTableWidgetItem *monitorPathItem = new QTableWidgetItem(firmware.monitorPath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 5, monitorPathItem);

        QTableWidgetItem *monitorSoftItem = new QTableWidgetItem(QString::number(firmware.monitorSoftVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 6, monitorSoftItem);

        QTableWidgetItem *tehMonitorPathItem = new QTableWidgetItem(firmware.tehMonitorPath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 7, tehMonitorPathItem);

        QTableWidgetItem *loaderSoftItem = new QTableWidgetItem(QString::number(firmware.loaderSoftVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 8, loaderSoftItem);

        QTableWidgetItem *loaderPathItem = new QTableWidgetItem(firmware.loaderPath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 9, loaderPathItem);
    }
    else
    {
        QList<QTableWidgetItem *> list = ui->firmwareTable->findItems(firmwareNameMap.value(firmware.deviceType),Qt::MatchExactly);
        foreach (QTableWidgetItem *item, list) {
            int groupCount = ui->firmwareTable->item(item->row(), 1)->text().toInt();
            int hardVersion = ui->firmwareTable->item(item->row(), 3)->text().toInt();

            qDebug() << groupCount << firmware.groupCount;
            if (firmware.groupCount == groupCount && firmware.hardVersion == hardVersion)
            {
                QTableWidgetItem *battlePathItem = ui->firmwareTable->item(item->row(), 2);
                battlePathItem->setText(firmware.battlePath);

                QTableWidgetItem *hardItem = ui->firmwareTable->item(item->row(), 3);
                hardItem->setText(QString::number(firmware.hardVersion));

                QTableWidgetItem *softItem = ui->firmwareTable->item(item->row(), 4);
                softItem->setText(QString::number(firmware.softVersion));

                QTableWidgetItem *monitorPathItem = ui->firmwareTable->item(item->row(), 5);
                monitorPathItem->setText(firmware.monitorPath);

                QTableWidgetItem *monitorSoftItem = ui->firmwareTable->item(item->row(), 6);
                monitorSoftItem->setText(QString::number(firmware.monitorSoftVersion));

                QTableWidgetItem *tehMonitorPathItem = ui->firmwareTable->item(item->row(), 7);
                tehMonitorPathItem->setText(firmware.tehMonitorPath);

                QTableWidgetItem *loaderSoftItem = new QTableWidgetItem(QString::number(firmware.loaderSoftVersion));
                ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 8, loaderSoftItem);

                QTableWidgetItem *loaderPathItem = ui->firmwareTable->item(item->row(), 9);
                loaderPathItem->setText(firmware.loaderPath);
            }
        }
    }

    selectedFirmwares.insert(firmware.uuid, firmware);
    selectFirmwareDialog->close();


}

void MainWindow::removeFirmware(QModelIndex index)
{
    QModelIndex firstIndex = index.sibling(index.row(), 0);
    QString uuid = ui->firmwareTable->item(firstIndex.row(),firstIndex.column())->data(Qt::UserRole).toString();
    selectedFirmwares.remove(uuid);
    ui->firmwareTable->removeRow(firstIndex.row());

}

void MainWindow::cleanOutput()
{
    QDir dir(destinationPath);
    dir.setFilter( QDir::NoDotAndDotDot | QDir::Files );
     foreach( QString dirItem, dir.entryList() )
         dir.remove( dirItem );

     dir.setFilter( QDir::NoDotAndDotDot | QDir::Dirs );
     foreach( QString dirItem, dir.entryList() )
     {
         QDir subDir( dir.absoluteFilePath( dirItem ) );
         subDir.removeRecursively();
     }
}

void MainWindow::on_openFirmwareDescroptionButton_released()
{

    QString fileName = QFileDialog::getOpenFileName(this,
          tr("Открыть"), "/home", tr("Json Files (*.json)"));

    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QByteArray val;
        val = file.readAll();
        file.close();

        QJsonDocument document = QJsonDocument::fromJson(val);
        if  (document.isArray())
        {
            QJsonArray array = document.array();
            selectedFirmwares.clear();
            foreach (const QJsonValue & v, array)
            {
                SelectFirmwareDialog::Firmware firmware;
                firmware.uuid = QUuid::createUuid().toString();
                firmware.deviceType = v.toObject().value("deviceType").toInt();
                firmware.groupCount = v.toObject().value("groupCount").toInt();
                firmware.needMonitor = v.toObject().value("needMonitor").toBool();
                firmware.localBattlePath = v.toObject().value("battlePath").toString();
                firmware.battlePath = v.toObject().value("systemBattlePath").toString();
                firmware.hardVersion = v.toObject().value("hardVersion").toInt();
                firmware.softVersion = v.toObject().value("softVersion").toInt();
                firmware.localMonitorPath = v.toObject().value("monitorPath").toString();
                firmware.monitorPath = v.toObject().value("systemMonitorPath").toString();
                firmware.monitorSoftVersion = v.toObject().value("monitorSoftVersion").toInt();
                firmware.localTehMonitorPath = v.toObject().value("tehMonitorPath").toString();
                firmware.tehMonitorPath = v.toObject().value("systemTehMonitorPath").toString();
                firmware.loaderSoftVersion = v.toObject().value("loaderSoftVersion").toInt();
                firmware.localLoaderPath = v.toObject().value("loaderPath").toString();
                firmware.loaderPath = v.toObject().value("systemLoaderPath").toString();
                selectedFirmwares.insert(firmware.uuid, firmware);
            }
        }

        showOpenedFirmwares();
    }
}

void MainWindow::showOpenedFirmwares()
{
    auto iter = selectedFirmwares.begin();
    for (; iter != selectedFirmwares.end(); ++iter)
    {
        SelectFirmwareDialog::Firmware firmware = iter.value();

        ui->firmwareTable->setRowCount(ui->firmwareTable->rowCount()+1);
        QTableWidgetItem *typeItem = new QTableWidgetItem(firmwareNameMap.value(firmware.deviceType));
        typeItem->setData(Qt::UserRole, firmware.uuid);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 0, typeItem);

        QTableWidgetItem *groupItem = new QTableWidgetItem(QString::number(firmware.groupCount));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 1, groupItem);

        QTableWidgetItem *battlepathItem = new QTableWidgetItem(firmware.battlePath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 2, battlepathItem);

        QTableWidgetItem *hardItem = new QTableWidgetItem(QString::number(firmware.hardVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 3, hardItem);

        QTableWidgetItem *softItem = new QTableWidgetItem(QString::number(firmware.softVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 4, softItem);

        QTableWidgetItem *monitorPathItem = new QTableWidgetItem(firmware.monitorPath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 5, monitorPathItem);

        QTableWidgetItem *monitorSoftItem = new QTableWidgetItem(QString::number(firmware.monitorSoftVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 6, monitorSoftItem);

        QTableWidgetItem *tehMonitorPathItem = new QTableWidgetItem(firmware.tehMonitorPath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 7, tehMonitorPathItem);

        QTableWidgetItem *loaderSoftItem = new QTableWidgetItem(QString::number(firmware.loaderSoftVersion));
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 8, loaderSoftItem);

        QTableWidgetItem *loaderPathItem = new QTableWidgetItem(firmware.loaderPath);
        ui->firmwareTable->setItem(ui->firmwareTable->rowCount() - 1, 9, loaderPathItem);

    }

}
